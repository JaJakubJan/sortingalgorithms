import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class GUI extends JFrame {
    private JButton bquit;
    private JPanel mainPanel;
    private JButton bquicksort;
    private JButton bshellsort;
    private JButton bmergesort;
    private JButton binsertsort;
    private JButton bgenerate;
    private JComboBox box;
    private JLabel lsupp1;
    private JLabel lsize;
    private JLabel lbound;
    private JLabel ldistance;
    private JLabel lcommunicate;
    private JTextField tsize;
    private JTextField tdistance;
    private JTextField tbound;

    private long time, flag;

    public GUI() {
        binsertsort.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int[] array = generate();
                if (flag != 1) {
                    SortingAlg s = new SortingAlg();
                    long tempTime = System.currentTimeMillis();
                    s.insertionSort(array);
                    time = System.currentTimeMillis() - tempTime;
                    JOptionPane.showMessageDialog(rootPane, "Tablica posortowana\nMetoda: Insert Sort\nTyp: " + box.getItemAt(box.getSelectedIndex()).toString() + "\nRozmiar: " + tsize.getText() + "\nZakres: " + tbound.getText() + "\nCzas sortowania: " + time + " ms");
                }
            }
        });
        bmergesort.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int[] array = generate();
                if (flag != 1) {
                    SortingAlg s = new SortingAlg();
                    long tempTime = System.currentTimeMillis();
                    s.mergeSort(array);
                    time = System.currentTimeMillis() - tempTime;
                    JOptionPane.showMessageDialog(rootPane, "Tablica posortowana\nMetoda: Merge Sort\nTyp: " + box.getItemAt(box.getSelectedIndex()).toString() + "\nRozmiar: " + tsize.getText() + "\nZakres: " + tbound.getText() + "\nCzas sortowania: " + time + " ms");
                }
            }
        });
        bquicksort.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int[] array = generate();
                if (flag != 1) {
                    SortingAlg s = new SortingAlg();
                    long tempTime = System.currentTimeMillis();
                    s.quickSort(array);
                    time = System.currentTimeMillis() - tempTime;
                    JOptionPane.showMessageDialog(rootPane, "Tablica posortowana\nMetoda: Quick Sort\nTyp: " + box.getItemAt(box.getSelectedIndex()).toString() + "\nRozmiar: " + tsize.getText() + "\nZakres: " + tbound.getText() + "\nCzas sortowania: " + time + " ms");
                }
            }
        });
        bshellsort.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int[] array = generate();
                try {
                    if ((Integer.parseInt(tdistance.getText()) < 0 || Integer.parseInt(tdistance.getText()) > Integer.parseInt(tsize.getText()))) {
                        flag = 1;
                        JOptionPane.showMessageDialog(rootPane, "Błąd przy generowaniu tablicy");
                    }
                } catch (Exception exp) {
                    flag = 1;
                    JOptionPane.showMessageDialog(rootPane, "Błąd przy generowaniu tablicy");
                }
                if (flag != 1) {
                    SortingAlg s = new SortingAlg();
                    long tempTime = System.currentTimeMillis();
                    s.shellSort(array, Integer.parseInt(tdistance.getText()));
                    time = System.currentTimeMillis() - tempTime;
                    JOptionPane.showMessageDialog(rootPane, "Tablica posortowana\nMetoda: Shell Sort\nTyp: " + box.getItemAt(box.getSelectedIndex()).toString() + "\nRozmiar: " + tsize.getText() + "\nZakres: " + tbound.getText() + "\nCzas sortowania: " + time + " ms");
                }
            }
        });
        bquit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("GUI");
        frame.setContentPane(new GUI().mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private int[] generate() {
        flag = 0;
        int[] array = new int[1];
        try {
            SortingAlg s = new SortingAlg();
            if (Integer.parseInt(tsize.getText()) > 0 && Integer.parseInt(tbound.getText()) > 0) {
                array = new int[Integer.parseInt(tsize.getText())];
                Random r = new Random();
                for (int i = 0; i < array.length; i++)
                    array[i] = r.nextInt(Integer.parseInt(tbound.getText()));
                if (box.getItemAt(box.getSelectedIndex()).toString().compareTo("posortowany w 50%") == 0) {
                    array = s.quickSort(array);
                    for (int i = (array.length / 2); i < array.length; i++)
                        array[i] = r.nextInt(Integer.parseInt(tbound.getText()));
                } else if (box.getItemAt(box.getSelectedIndex()).toString().compareTo("posortowany") == 0) {
                    array = s.quickSort(array);
                } else if (box.getItemAt(box.getSelectedIndex()).toString().compareTo("posortowany odwrotnie") == 0) {
                    int[] tempArray = s.quickSort(array);
                    for (int i = 0; i < array.length; i++)
                        array[i] = tempArray[array.length - i - 1];
                }
            } else
                throw new NumberFormatException();
        } catch (NumberFormatException exp) {
            JOptionPane.showMessageDialog(rootPane, "Błąd przy generowaniu tablicy");
            flag = 1;
        }
        return array;
    }
}