import org.junit.Test;
import org.junit.Before;
import java.util.Random;

public class TestClass {
    private int arrayBound;
    private int[] array;
    private SortingAlg s;
    private Random r;

    @Before
    public void before(){
        int arraySize = 500000;
        arrayBound = 100;

        array = new int[arraySize];
        s = new SortingAlg();
        r = new Random();
    }

    @Test
    public void testMergeSort(){
        for (int i = 0; i < array.length; i++)
            array[i] = r.nextInt(arrayBound);
        array = s.mergeSort(array);
        for(int i = 1; i < array.length ; i++)
            if (array[i - 1] > array[i])
                assert false : "Nieposortowane liczby!";
    }

    @Test
    public void testInsertionSort(){
        for (int i = 0; i < array.length; i++)
            array[i] = r.nextInt(arrayBound);
        array = s.insertionSort(array);
        for(int i = 1; i < array.length ; i++)
            if (array[i - 1] > array[i])
                assert false : "Nieposortowane liczby!";
    }


    @Test
    public void testQuickSort(){
        for (int i = 0; i < array.length; i++)
            array[i] = r.nextInt(arrayBound);
        s.quickSort(array);
        for(int i = 1; i < array.length ; i++)
            if (array[i - 1] > array[i])
                assert false : "Nieposortowane liczby!";
    }
    @Test
    public void testShellSort(){
        for (int i = 0; i < array.length; i++)
            array[i] = r.nextInt(arrayBound);
        s.shellSort(array, array.length/2);
        for(int i = 1; i < array.length ; i++)
            if (array[i - 1] > array[i])
                assert false : "Nieposortowane liczby!";
    }


}
